import React from "react";
import ReactDOM from 'react-dom';

function Signs () {
  return (
    <div>
      <table>
        <tbody>
          <tr>
            <td colspan="3"><h3>Знаки Зодіака</h3></td>
          </tr>
          <tr>
            <td>Овен</td>
            <td>&#9800;</td>
            <td>21.03-19.04</td>
          </tr>
          <tr>
            <td>Телець</td>
            <td>&#9801;</td>
            <td>20.04-20.05</td>
          </tr>
          <tr>
            <td>Близнюки</td>
            <td>&#9802;</td>
            <td>21.05-20.06</td>
          </tr>
            
            <tr>
              <td>Рак</td>
              <td>&#9803;</td>
              <td>21.06-22.07</td>
            </tr>
            <tr>
              <td>Лев</td>
              <td>&#9804;</td>
              <td>23.07-22.08</td>
            </tr>
            <tr>
              <td>Діва</td>
              <td>&#9805;</td>
              <td>23.08-22.09</td>
            </tr>
            <tr>
              <td>Терези</td>
              <td>&#9806;</td>
              <td>23.09-22.10</td>
            </tr>
            <tr>
              <td>Скорпіон</td>
              <td>&#9807;</td>
              <td>23.10-21.11</td>
            </tr>
            <tr>
            <td>Стрілець</td>
              <td>&#9808;</td>
              <td>22.11-21.12</td>
            </tr>
            <tr>
              <td>Козеріг</td>
              <td>&#9809;</td>
              <td>22.12-19.01</td>
            </tr>
            <tr>
              <td>Водолій</td>
              <td>&#9810;</td>
              <td>20.01-18.02</td> 
            </tr>
            <tr>
              <td>Риби</td>  
              <td>&#9811;</td>
              <td>19.02-20.03</td>
            </tr>
        </tbody>
      </table>
    </div>
    )  
}  
  
ReactDOM.render(<Signs></Signs>, document.querySelector("#one"))