import React from "react";
import Profile from "../profile/profile";
import Search from "../search/search";
import Board from "../board/board";
import Ellipse from "../ellipse/ellipse";



function Nav() {
    return (
        <div className="navigation">
              <Profile />
              <Search />
              <Board />
              <Ellipse />
        </div>
        
    )
    
} 


export default Nav;





