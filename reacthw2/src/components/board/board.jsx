import React from "react";
import Footer from "../footer/footer";

function Board(){
    return(
        <div className="board">
                <div className="board-menu menu">
                  <div className="input"><input type="image" src="./img/dashboard.svg" alt="Dashboard"/></div>
                  <a href="#"  className="board-text">Dashboard</a>
                </div>
                <div className="board-menu menu">
                  <div className="input"><input type="image" src="./img/revenue.svg" alt="Revenue"/></div>
                  <a href="#" className="board-text">Revenue</a>
                </div>
                <div className="board-menu menu">
                  <div className="input"><input type="image" src="./img/notification.svg" alt="Notifications"/></div>
                  <a href="#"  className="board-text">Notifications</a>
                </div>
                <div className="board-menu menu">
                  <div className="input"><input type="image" src="./img/analytics.svg" alt="Analytics"/></div>
                  <a href="#" className="board-text">Analytics</a>
                </div>
                <div className="board-menu menu">
                  <div className="input"><input type="image" src="./img/inventory.svg" alt="Inventory"/></div>
                  <a href="#"className="board-text">Inventory</a>
                </div>
                
              <Footer /> 
              </div>
    )
}

export default Board;