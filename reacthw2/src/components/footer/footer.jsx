import React from "react";

function Footer() {
    return (
        <footer>
            <div className="board-menu menu board-menu-logout">
                <div className="input"><input type="image" src="./img/logout.svg" alt="Logout"/></div>
                <a href="#" className="board-text ">Logout</a>
            </div>
            <div className="board-menu board-menu-light">
                <div className="input"><input type="image" src="./img/light.svg" alt="Light mode"/></div>
                <div className="board-text">Light mode</div>

                <div className="board-mode">
                    <div className="radio-light">
                        <input id='light' type="radio" name='mode'  checked/>	
                        <label for='light'><div className='img-light light'><img src="./img/sunlight.svg" alt="light mode" /></div></label>
                    </div>
                    <div className="radio-dark">
                        <input id='dark' type="radio" name='mode'/>	
                        <label for='dark'><div className='img-dark dark'><img src="./img/dark.svg" alt="dark mode" /></div></label>
                    </div>
                </div>
            </div>
        </footer>
    )
    }

  export default Footer;

