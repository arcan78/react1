import React from "react";

function Search() {
    return (
        <div className="container-search">
            <input type="image" src="./img/search.svg" alt="Кнопка «input»"/>
            <input type="text" placeholder="Search..." name="search"/>
        </div>
    )
}

  export default Search;